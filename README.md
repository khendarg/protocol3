# protocol3

Prepare fxp/p2 output for profile-to-profile comparisons

## Instructions

1. Run `collect_p2pairs.py` on a famXpander directory or a CD-HIT results directory and a protocol2 directory to pull the cluster vs. cluster mappings

1. Fetch relevant sequences from NCBI and place them in an appropriate directory

1. Run `run_mafft.py` on the sequences

1. Run `run_hhalign.py` on the mappings from `collect_p2pairs.py` and the MSAs generated by `run_mafft.py`
