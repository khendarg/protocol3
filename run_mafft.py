#!/usr/bin/env python

import io
import os
import sys
import argparse
import subprocess
import tempfile
from Bio import SeqIO, AlignIO, Align

STRICT = False
def warn(*things): print('[WARNING]', *things, file=sys.stderr)

def parse_pairfile(fh):
	pairs = set()
	for l in fh:
		if l.startswith('#'): continue
		elif not l.strip(): continue

		sl = l.rstrip().split('\t')
		pairs.add(tuple(sl[:2]))
	return pairs

def get_seqdict(seqlist):
	seqdict = {}
	for name in seqlist:
		k, v = name.split('/')
		if k in seqdict: seqdict[k].append(v)
		else: seqdict[k] = [v]

	return seqdict

def run_mafft(seqlist, method=None, mafftargs=None):
	#place sequences in tempfile before aligning
	#this conveniently generalizes for subsets of sequence libraries and such

	if len(seqlist) == 1:
		msa = Align.MultipleSeqAlignment(seqlist)
		return [msa]

	seqfile = tempfile.NamedTemporaryFile('w', suffix='.faa')
	SeqIO.write(seqlist, seqfile, 'fasta')
	seqfile.flush()

	cmd = ['mafft']

	if method is None: pass
	elif method == 'linsi': cmd.extend(['--localpair', '--maxiterate', '1000'])
	elif method == 'ginsi': cmd.extend(['--globalpair', '--maxiterate', '1000'])
	elif method == 'einsi': cmd.extend(['--ep', '0', '--genafpair', '--maxiterate', '1000'])
	elif method == 'fftnsi': cmd.extend(['--retree', '2', '--maxiterate', '1000'])
	elif method == 'fftns': cmd.extend(['--retree', '2', '--maxiterate', '0'])
	elif method == 'nwnsi': cmd.extend(['--retree', '2', '--maxiterate', '2', '--nofft'])
	elif method == 'nwns': cmd.extend(['--retree', '2', '--maxiterate', '0', '--nofft'])
	else: raise ValueError('Unknown method: "{}"'.format(method))

	if mafftargs: cmd.extend(mafftargs)

	if '--clustalout' in cmd: outfmt = 'clustal'
	else: outfmt = 'fasta'

	cmd.append(seqfile.name)

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()

	mafftio = io.StringIO(out.decode('utf-8'))
	return AlignIO.parse(mafftio, outfmt)

def bulk_mafft(seqdir, seqdict, method=None, mafftargs=None, outdir=None, force=False):
	if outdir and not os.path.isdir(outdir): os.mkdir(outdir)

	total = sum([len(seqdict[fam]) for fam in seqdict])
	index = 0
	for fam in sorted(seqdict):
		if outdir:
			outprefix = '{}/{}'.format(outdir, fam)
			if not os.path.isdir(outprefix): 
				os.mkdir(outprefix)

		for clname in sorted(seqdict[fam]):
			index += 1
			prefix = '{}/{}/{}'.format(seqdir, fam, clname)
			found = False
			outfn = '{}/{}.aln'.format(outprefix, clname)
			if outdir and (not force) and os.path.isfile(outfn): continue

			for suffix in ('fa', 'fasta', 'faa'):
				fn = '{}.{}'.format(prefix, suffix)

				if os.path.isfile(fn):
					found = True
					break

			if not found: 
				if STRICT: raise FileNotFoundError('Could not find a file matching {}'.format(prefix))
				else: warn('Could not find a file matching {}'.format(prefix))
				continue

			sequences = list(SeqIO.parse(fn, 'fasta'))

			seqlen = [len(record.seq) for record in sequences]
			meanlen = sum(seqlen)/len(seqlen)
			print('Running MAFFT on #{}/{} ({}/{}): {} sequences ({:0.0f} long on average)'.format(index, total, fam, clname, len(seqlen), meanlen))
			msas = run_mafft(sequences, method=method, mafftargs=mafftargs)

			if outdir: AlignIO.write(msas, outfn, 'clustal')
			


def main(pairfh, seqdir, method=None, mafftargs=None, outdir=None):
	pairs = parse_pairfile(pairfh)
	relevant_seqs = set([p[0] for p in pairs] + [p[1] for p in pairs])
	seqdict = get_seqdict(relevant_seqs)

	bulk_mafft(seqdir, seqdict, method, mafftargs, outdir=outdir)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('pairs', type=argparse.FileType('r'), help='File containing tab-separated family/cluster (e.g. 1.X.1/Cluster_1')
	parser.add_argument('seqdir', help='Directory containing CD-HIT clusters (should be named something like 1.X.1/Cluster_4.faa)')
	parser.add_argument('-o', required=True, help='Where to write the alignments')
	parser.add_argument('--method', help='Use one of linsi, ginsi, einsi, fftnsi, fftns, nwnsi, nwns')
	parser.add_argument('--mafftargs', help='Extra arguments for MAFFT')

	args = parser.parse_args()

	main(args.pairs, args.seqdir, args.method, args.mafftargs, outdir=args.o)
