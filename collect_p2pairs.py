#!/usr/bin/env python

import os
import argparse

def parse_clstr(fh, label, strip_version=True):
	current = None
	clusters = {}
	for l in fh:
		if l.startswith('>'): current = l[1:-1].replace(' ', '_')
		elif '>' in l:
			acc = l[l.find('>')+1:l.find('...')]
			if strip_version:
				if acc.count('.') == 1: acc = acc[:acc.find('.')]

			clusters[acc] = (label, current)

	return clusters

def get_clusters(cdhitdir, fxp=False):
	clusters = {}

	if fxp:
		for fam in os.listdir(cdhitdir):
			fn = '{}/{}/results.faa.cdhit.clstr'.format(cdhitdir, fam)

			if not os.path.isfile(fn): 
				print('[WARNING]:', 'Could not find {}'.format(fn))
				continue

			with open(fn) as fh: clusters.update(parse_clstr(fh, fam))

	else:
		for bn in os.listdir(cdhitdir):
			if not bn.endswith('.clstr'): continue

			fam = bn.replace('.clstr', '')
			fn = '{}/{}'.format(cdhitdir, bn)

			with open(fn) as fh: clusters.update(parse_clstr(fh, fam))

	return clusters

def parse_p2tbl(fh):
	pairs = []
	for l in fh:
		if l.startswith('#'): continue
		elif l.startswith('Subject ID'): continue
		elif not l.strip(): continue

		else:
			pairs.append(l.split()[:2])

	return pairs

def get_p2pairs(p2dir):
	pairs = []
	for bn in os.listdir(p2dir):
		if '_vs_' not in bn: continue

		prefix = '{}/{}'.format(p2dir, bn)
		with open('{}/report.tbl'.format(prefix)) as fh:
			pairs.extend(parse_p2tbl(fh))

	return pairs

def resolve_agenda(p2pairs, clusters):
	alignme = set()
	missing = 0
	for pair in p2pairs:
		try: alignme.add((clusters[pair[0]], clusters[pair[1]]))
		except KeyError: missing += 1

	print('{} total, {} missing'.format(len(p2pairs), missing))

	return alignme

def write_alignme(alignme, outfh):
	for (fam1, cl1), (fam2, cl2) in alignme:
		outfh.write('{}/{}\t{}/{}\n'.format(fam1, cl1, fam2, cl2))

def main(cdhitdir, p2dir, outfh, fxp=False):
	clusters = get_clusters(cdhitdir, fxp=fxp)
	p2pairs = get_p2pairs(p2dir)

	alignme = resolve_agenda(p2pairs, clusters)

	write_alignme(alignme, outfh)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	p1dir = parser.add_mutually_exclusive_group(required=True)
	p1dir.add_argument('--cdhitdir', help='Directory containing CD-HIT .clstr files. Useful for factoring in reclusterings')
	p1dir.add_argument('--fxpdir', help='Directory containing all family famXpander results')
	parser.add_argument('p2dir', help='Directory containing all family vs. family p2 results')
	parser.add_argument('-o', required=True, type=argparse.FileType('w'), help='Where to write the alignments')

	args = parser.parse_args()

	if args.cdhitdir is None: main(args.fxpdir, args.p2dir, args.o, fxp=True)
	else: main(args.cdhitdir, args.p2dir, args.o)
	#main(args.cdhitdir, args.p2dir, args.o)
